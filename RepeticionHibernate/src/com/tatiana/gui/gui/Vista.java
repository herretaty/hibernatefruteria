package com.tatiana.gui.gui;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;

public class Vista extends JFrame{

    private JPanel panel1;
    private JTabbedPane tabbedPane1;
    //INFORMACION//
    JButton altaButton;
    JButton listarProductosButton;
    JButton modificarButton;
    JButton borrarButton;
    JButton listarProveedoresButton;
    JList listProductos;
    JList listProveedores;
    JList listProductoProveedor;


    JTextField txtId;
    JTextField txtCodigo;
    JTextField txtOrigen;
    JTextField txtPrecio;
    JTextField txtPeso;
    JTextField txtProveedor;
    DateTimePicker dateTimePicker;

    DefaultListModel dlm;
    DefaultListModel dlmProveedores;
    DefaultListModel dlmProductosProveedor;

    //DATOS//


    JMenuItem conexionItem;
    JMenuItem salirItem;


}
