package com.tatiana.gui.gui;

import com.tatiana.gui.Producto;
import com.tatiana.gui.Proveedor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;

public class Modelo {
    SessionFactory sessionFactory;

    public void desconectar() {
        if (sessionFactory!= null && sessionFactory.isOpen()){
            sessionFactory.close();
        }

    }

    public void conectar() {
        Configuration configuracion = new Configuration();
        configuracion.configure("hibernate.cfg.xml");

        configuracion.addAnnotatedClass(Producto.class);
        configuracion.addAnnotatedClass(Proveedor.class);

        configuracion.addAnnotatedClass
        sessionFactory= configuracion.buildSessionFactory(ssr);

    }

    public void altaProducto(Producto nuevoProducto) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoProducto);
        sesion.getTransaction().commit();

        sesion.close();
    }
    public ArrayList<Producto>getProductos(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Producto ") ;
        ArrayList<Producto>lista= (ArrayList<Producto>)query.getResultList();
        sesion.close();
        return lista;
    }

    public void modificar(Producto productoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(productoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrar(Producto productoBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(productoBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }
    public ArrayList<Proveedor>getProveedores(){
        Session sesion = sessionFactory.openSession();
        Query query= sesion.createQuery("FROM Proveedor ");
        ArrayList<Proveedor> listaProveedores= (ArrayList<Proveedor>)query.getResultList();
        sesion.close();
        return listaProveedores;
    }

    public ArrayList<Producto>getProductosProveedor( Proveedor proveedorSeleccionado){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Producto WHERE proveedores= :prove");
        query.setParameter("prove", proveedorSeleccionado);
        ArrayList<Producto> lista= (ArrayList<Producto>)query.getResultList();
        return lista;

    }


    private class addAnnotatedClass {
    }
}
