package com.tatiana.gui.gui;

import com.tatiana.gui.Producto;
import com.tatiana.gui.Proveedor;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.bind.Element;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Controlador implements ActionListener, ListSelectionListener {

    private Vista vista;
    private Modelo modelo;



    public Controlador(Vista vista, Modelo modelo) {
        this.vista= vista;
        this.modelo= modelo;

        addActionListeners(this);
        addListSelectionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch(comando){
            case "Salir":
                modelo.desconectar();
                System.exit(0);
                break;
            case "Conectar":
                vista.conexionItem.setEnabled(false);
                modelo.conectar();
                break;
            case "Alta":
                Producto nuevoProducto= new Producto();;
                nuevoProducto.setCodigo(vista.txtCodigo.getText());
                nuevoProducto.setOrigen(vista.txtOrigen.getText());
                nuevoProducto.setPrecio(Double.valueOf(vista.txtPrecio.getText()));
                nuevoProducto.setPeso(vista.txtPeso.getText());
                nuevoProducto.setFechaCaducidad(Timestamp.valueOf(vista.dateTimePicker.getDateTimePermissive()));
                modelo.altaProducto(nuevoProducto);
                break;
            case "Listar":
                listarProductos(modelo.getProductos());
                break;
            case "Modificar":
                Producto productoSeleccion= (Producto)vista.listProductos.getSelectedValue();
                productoSeleccion.setCodigo(vista.txtCodigo.getText());
                productoSeleccion.setOrigen(vista.txtOrigen.getText());
                productoSeleccion.setPrecio(Double.valueOf(vista.txtPrecio.getText()));
                productoSeleccion.setFechaCaducidad(Timestamp.valueOf(vista.dateTimePicker.getDateTimePermissive()));
                productoSeleccion.setProveedores((Proveedor) vista.listProveedores.getSelectedValue());
                System.out.println((Proveedor)vista.listProveedores.getSelectedValue());
                modelo.modificar(productoSeleccion);
                break;
            case "Borrar":
                Producto productoBorrado= (Producto)vista.listProductos.getSelectedValue();
                modelo.borrar(productoBorrado);
                break;
            case "listaProveedores":
                listarProveedores(modelo.getProveedores());
                break;
        }
        listarProductos(modelo.getProductos());
    }




    private void listarProveedores(ArrayList<Proveedor> proveedores) {
        vista.dlmProveedores.clear();
        for (Proveedor proveedor: proveedores){
            vista.dlmProveedores.addElement(proveedor);
        }
    }

    public void listarProductos(ArrayList<Producto> lista) {
        vista.dlm.clear();
        for (Producto unProducto: lista){
            vista.dlm.addElement(unProducto);
        }
    }

    public void listarProductosProveedor(List<Producto> lista){
        vista.dlmProductosProveedor.clear();
        for (Producto unproducto: lista){
            vista.dlmProductosProveedor.addElement(unproducto);
        }
    }

    private void addActionListeners(ActionListener listener){
        vista.conexionItem.addActionListener(listener);
        vista.salirItem.addActionListener(listener);
        vista.altaButton.addActionListener(listener);
        vista.borrarButton.addActionListener(listener);
        vista.modificarButton.addActionListener(listener);
        vista.listarProductosButton.addActionListener(listener);
        vista.listarProveedoresButton.addActionListener(listener);
    }
    private void addListSelectionListener(ListSelectionListener listener){
        vista.listProductos.addListSelectionListener(listener);
        vista.listProveedores.addListSelectionListener(listener);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()){
            if (e.getSource()== vista.listProductos){
                Producto podructoSeleccion= (Producto) vista.listProductos.getSelectedValue();
                vista.txtId.setText(String.valueOf(podructoSeleccion.getId()));
                vista.txtCodigo.setText(podructoSeleccion.getCodigo());
                vista.txtOrigen.setText(podructoSeleccion.getOrigen());
                vista.txtPrecio.setText(String.valueOf(podructoSeleccion.getPrecio()));
                vista.txtPeso.setText(podructoSeleccion.getPeso());
                vista.dateTimePicker.setDateTimePermissive(podructoSeleccion.getFechaCaducidad().toLocalDateTime());
                if (podructoSeleccion.getProveedores()!=null){
                    vista.txtProveedor.setText(podructoSeleccion.getProveedores().toString());
                }else {
                    vista.txtProveedor.setText("");
                }
            }else{
                if (e.getSource()== vista.listProveedores){
                    Proveedor proveedorSeleccionado= (Proveedor) vista.listProveedores.getSelectedValue();
                    listarProductosProveedor(modelo.getProductosProveedor(proveedorSeleccionado));
                }
            }
        }

    }
}