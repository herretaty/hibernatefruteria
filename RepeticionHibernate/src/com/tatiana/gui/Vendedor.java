package com.tatiana.gui;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "vendedores", schema = "fruteriah", catalog = "")
public class Vendedor {
    private int id;
    private String nombre;
    private String apellidos;
    private String direccion;
    private String telefono;
    private String mail;
    private List<Venta> ventas;
    private List<VendedorProveedor> entregas;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "mail")
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vendedor vendedor = (Vendedor) o;
        return id == vendedor.id &&
                Objects.equals(nombre, vendedor.nombre) &&
                Objects.equals(apellidos, vendedor.apellidos) &&
                Objects.equals(direccion, vendedor.direccion) &&
                Objects.equals(telefono, vendedor.telefono) &&
                Objects.equals(mail, vendedor.mail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, direccion, telefono, mail);
    }

    @OneToMany(mappedBy = "vendedor")
    public List<Venta> getVentas() {
        return ventas;
    }

    public void setVentas(List<Venta> ventas) {
        this.ventas = ventas;
    }

    @OneToMany(mappedBy = "vendedor")
    public List<VendedorProveedor> getEntregas() {
        return entregas;
    }

    public void setEntregas(List<VendedorProveedor> entregas) {
        this.entregas = entregas;
    }
}
