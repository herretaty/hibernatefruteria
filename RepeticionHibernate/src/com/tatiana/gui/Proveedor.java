package com.tatiana.gui;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "proveedores", schema = "fruteriah", catalog = "")
public class Proveedor {
    private int id;
    private String codigo;
    private String nombre;
    private String direccion;
    private String variedadProducto;
    private Timestamp fechaOfertas;
    private List<VendedorProveedor> entregas;
    private List<Producto> productos;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "variedad_producto")
    public String getVariedadProducto() {
        return variedadProducto;
    }

    public void setVariedadProducto(String variedadProducto) {
        this.variedadProducto = variedadProducto;
    }

    @Basic
    @Column(name = "fecha_ofertas")
    public Timestamp getFechaOfertas() {
        return fechaOfertas;
    }

    public void setFechaOfertas(Timestamp fechaOfertas) {
        this.fechaOfertas = fechaOfertas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Proveedor proveedor = (Proveedor) o;
        return id == proveedor.id &&
                Objects.equals(codigo, proveedor.codigo) &&
                Objects.equals(nombre, proveedor.nombre) &&
                Objects.equals(direccion, proveedor.direccion) &&
                Objects.equals(variedadProducto, proveedor.variedadProducto) &&
                Objects.equals(fechaOfertas, proveedor.fechaOfertas);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, codigo, nombre, direccion, variedadProducto, fechaOfertas);
    }

    @OneToMany(mappedBy = "proveedor")
    public List<VendedorProveedor> getEntregas() {
        return entregas;
    }

    public void setEntregas(List<VendedorProveedor> entregas) {
        this.entregas = entregas;
    }

    @ManyToMany
    @JoinTable(name = "proveedor_producto", catalog = "", schema = "fruteriah", joinColumns = @JoinColumn(name = "id_producto", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_proveedor", referencedColumnName = "id", nullable = false))
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }
}
