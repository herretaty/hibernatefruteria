package com.tatiana.gui;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "ventas", schema = "fruteriah", catalog = "")
public class Venta {
    private int id;
    private Double totalPagar;
    private String albaran;
    private Byte finalizacion;
    private Vendedor vendedor;
    private List<DetalleProducto> detalles;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "total_pagar")
    public Double getTotalPagar() {
        return totalPagar;
    }

    public void setTotalPagar(Double totalPagar) {
        this.totalPagar = totalPagar;
    }

    @Basic
    @Column(name = "albaran")
    public String getAlbaran() {
        return albaran;
    }

    public void setAlbaran(String albaran) {
        this.albaran = albaran;
    }

    @Basic
    @Column(name = "finalizacion")
    public Byte getFinalizacion() {
        return finalizacion;
    }

    public void setFinalizacion(Byte finalizacion) {
        this.finalizacion = finalizacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Venta venta = (Venta) o;
        return id == venta.id &&
                Objects.equals(totalPagar, venta.totalPagar) &&
                Objects.equals(albaran, venta.albaran) &&
                Objects.equals(finalizacion, venta.finalizacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, totalPagar, albaran, finalizacion);
    }

    @ManyToOne
    @JoinColumn(name = "id_vendedor", referencedColumnName = "id", nullable = false)
    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    @OneToMany(mappedBy = "venta")
    public List<DetalleProducto> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<DetalleProducto> detalles) {
        this.detalles = detalles;
    }
}
