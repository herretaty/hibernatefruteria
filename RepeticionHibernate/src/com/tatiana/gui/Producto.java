package com.tatiana.gui;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "productos", schema = "fruteriah", catalog = "")
public class Producto {
    private int id;
    private String codigo;
    private String origen;
    private Double precio;
    private String peso;
    private Timestamp fechaCaducidad;
    private List<Proveedor> proveedores;
    private List<DetalleProducto> detalles;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Basic
    @Column(name = "origen")
    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    @Basic
    @Column(name = "precio")
    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "peso")
    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    @Basic
    @Column(name = "fecha_caducidad")
    public Timestamp getFechaCaducidad() {
        return fechaCaducidad;
    }

    public void setFechaCaducidad(Timestamp fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Producto producto = (Producto) o;
        return id == producto.id &&
                Objects.equals(codigo, producto.codigo) &&
                Objects.equals(origen, producto.origen) &&
                Objects.equals(precio, producto.precio) &&
                Objects.equals(peso, producto.peso) &&
                Objects.equals(fechaCaducidad, producto.fechaCaducidad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, codigo, origen, precio, peso, fechaCaducidad);
    }

    @ManyToMany(mappedBy = "proveedores")
    public List<Proveedor> getProveedores() {
        return proveedores;
    }

    public void setProveedores(Proveedor proveedores) {
        this.proveedores = (List<Proveedor>) proveedores;
    }

    @OneToMany(mappedBy = "producto")
    public List<DetalleProducto> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<DetalleProducto> detalles) {
        this.detalles = detalles;
    }
}
