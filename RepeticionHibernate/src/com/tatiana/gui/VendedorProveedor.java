package com.tatiana.gui;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "vendedor_proveedor", schema = "fruteriah", catalog = "")
public class VendedorProveedor {
    private int id;
    private Timestamp fechaCompra;
    private Vendedor vendedor;
    private Proveedor proveedor;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fecha_compra")
    public Timestamp getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(Timestamp fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VendedorProveedor entrega = (VendedorProveedor) o;
        return id == entrega.id &&
                Objects.equals(fechaCompra, entrega.fechaCompra);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fechaCompra);
    }

    @ManyToOne
    @JoinColumn(name = "id_vendedor", referencedColumnName = "id", nullable = false)
    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    @ManyToOne
    @JoinColumn(name = "id_proveedor", referencedColumnName = "id", nullable = false)
    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }
}
