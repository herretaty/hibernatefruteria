package com.tatiana.gui;

import com.tatiana.gui.gui.Controlador;
import com.tatiana.gui.gui.Modelo;
import com.tatiana.gui.gui.Vista;

public class Principal {

    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista,modelo);
    }
}



